-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: marketplace
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Inmueble`
--

DROP TABLE IF EXISTS `Inmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Inmueble` (
  `Id_Inmueble` int NOT NULL AUTO_INCREMENT,
  `Antiguedad` varchar(200) NOT NULL,
  `Num_Recamaras` int NOT NULL,
  `Num_Baños` int NOT NULL,
  `Costo` int NOT NULL,
  `Num_Niveles` int NOT NULL,
  `Cap_Estacionamiento` int NOT NULL,
  `Area` double NOT NULL,
  `Descripcion` varchar(200) NOT NULL,
  `persona_id_Persona` int NOT NULL,
  `Fecha_Registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `direccion_Id_Direccion` int NOT NULL,
  `tipoInmueble_Id_TipoInmueble` int NOT NULL,
  PRIMARY KEY (`Id_Inmueble`),
  KEY `fk_Inmueble_persona_idx` (`persona_id_Persona`),
  KEY `fk_Inmueble_direccion1_idx` (`direccion_Id_Direccion`),
  KEY `fk_Inmueble_tipoInmueble1_idx` (`tipoInmueble_Id_TipoInmueble`),
  CONSTRAINT `fk_Inmueble_direccion1` FOREIGN KEY (`direccion_Id_Direccion`) REFERENCES `direccion` (`Id_Direccion`),
  CONSTRAINT `fk_Inmueble_persona` FOREIGN KEY (`persona_id_Persona`) REFERENCES `persona` (`id_Persona`),
  CONSTRAINT `fk_Inmueble_tipoInmueble1` FOREIGN KEY (`tipoInmueble_Id_TipoInmueble`) REFERENCES `tipoInmueble` (`Id_TipoInmueble`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inmueble`
--

LOCK TABLES `Inmueble` WRITE;
/*!40000 ALTER TABLE `Inmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `Inmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `direccion` (
  `Id_Direccion` int NOT NULL AUTO_INCREMENT,
  `Estado` varchar(45) NOT NULL,
  `Municipio` varchar(150) NOT NULL,
  `Cp` int NOT NULL,
  `Cuidad` varchar(120) NOT NULL,
  `Colonia` varchar(120) NOT NULL,
  `Calle` varchar(120) NOT NULL,
  `No_Interior` int NOT NULL,
  `No_Exterior` int NOT NULL,
  `Latitud` decimal(10,0) NOT NULL,
  `Longitud` decimal(10,0) NOT NULL,
  `Referencias` varchar(200) DEFAULT NULL,
  `persona_id_Persona` int NOT NULL,
  PRIMARY KEY (`Id_Direccion`),
  KEY `fk_direccion_persona1_idx` (`persona_id_Persona`),
  CONSTRAINT `fk_direccion_persona1` FOREIGN KEY (`persona_id_Persona`) REFERENCES `persona` (`id_Persona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `multimedia` (
  `Id_Multimedia` int NOT NULL AUTO_INCREMENT,
  `Fecha_Subida` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Url` varchar(200) NOT NULL,
  `Inmueble_Id_Inmueble` int NOT NULL,
  `validacionMultimedia_Id_ValidacionMultimedia` int NOT NULL,
  PRIMARY KEY (`Id_Multimedia`),
  KEY `fk_multimedia_Inmueble1_idx` (`Inmueble_Id_Inmueble`),
  KEY `fk_multimedia_validacionMultimedia1_idx` (`validacionMultimedia_Id_ValidacionMultimedia`),
  CONSTRAINT `fk_multimedia_Inmueble1` FOREIGN KEY (`Inmueble_Id_Inmueble`) REFERENCES `Inmueble` (`Id_Inmueble`),
  CONSTRAINT `fk_multimedia_validacionMultimedia1` FOREIGN KEY (`validacionMultimedia_Id_ValidacionMultimedia`) REFERENCES `validacionMultimedia` (`Id_ValidacionMultimedia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `multimedia`
--

LOCK TABLES `multimedia` WRITE;
/*!40000 ALTER TABLE `multimedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `multimedia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `id_Persona` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(150) NOT NULL,
  `Apellidos` varchar(200) NOT NULL,
  `Fecha_Nacimiento` date NOT NULL,
  `Telefono` int NOT NULL,
  `Correo` varchar(100) NOT NULL,
  `Fecha_Registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tipoPersona_Id_TipoPersona` int NOT NULL,
  PRIMARY KEY (`id_Persona`),
  KEY `fk_persona_tipoPersona1_idx` (`tipoPersona_Id_TipoPersona`),
  CONSTRAINT `fk_persona_tipoPersona1` FOREIGN KEY (`tipoPersona_Id_TipoPersona`) REFERENCES `tipoPersona` (`Id_TipoPersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `Id_Status` int NOT NULL AUTO_INCREMENT,
  `Fecha_Registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Descripcion` int NOT NULL,
  PRIMARY KEY (`Id_Status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoInmueble`
--

DROP TABLE IF EXISTS `tipoInmueble`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoInmueble` (
  `Id_TipoInmueble` int NOT NULL AUTO_INCREMENT,
  `Descripcion` int NOT NULL,
  PRIMARY KEY (`Id_TipoInmueble`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoInmueble`
--

LOCK TABLES `tipoInmueble` WRITE;
/*!40000 ALTER TABLE `tipoInmueble` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoInmueble` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoPersona`
--

DROP TABLE IF EXISTS `tipoPersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoPersona` (
  `Id_TipoPersona` int NOT NULL AUTO_INCREMENT,
  `Descripcion` int NOT NULL,
  PRIMARY KEY (`Id_TipoPersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoPersona`
--

LOCK TABLES `tipoPersona` WRITE;
/*!40000 ALTER TABLE `tipoPersona` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipoPersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validacionMultimedia`
--

DROP TABLE IF EXISTS `validacionMultimedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `validacionMultimedia` (
  `Id_ValidacionMultimedia` int NOT NULL,
  `Url_Archivo` varchar(150) NOT NULL,
  `Fecha_Registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_Id_Status` int NOT NULL,
  PRIMARY KEY (`Id_ValidacionMultimedia`),
  KEY `fk_validacionMultimedia_status1_idx` (`status_Id_Status`),
  CONSTRAINT `fk_validacionMultimedia_status1` FOREIGN KEY (`status_Id_Status`) REFERENCES `status` (`Id_Status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validacionMultimedia`
--

LOCK TABLES `validacionMultimedia` WRITE;
/*!40000 ALTER TABLE `validacionMultimedia` DISABLE KEYS */;
/*!40000 ALTER TABLE `validacionMultimedia` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-10 16:33:51
